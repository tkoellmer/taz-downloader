import os
import datetime
import urllib.request as url

username = "abo-nummer"
password = "password"


base_url = "https://dl.taz.de/pdf/"

current_day = datetime.date.today().day
current_day_str = '{:02d}'.format(current_day)

download_url = base_url + current_day_str

print(download_url)

passwordMgr = url.HTTPPasswordMgrWithDefaultRealm()

passwordMgr.add_password(None, download_url, "%s" % username, "%s" % password)
auth_handler = url.HTTPBasicAuthHandler(passwordMgr)
url.install_opener(url.build_opener(auth_handler))

tazFile, headers = url.urlretrieve(download_url)

print(tazFile)

os.system("start " + tazFile)
